package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        return s.split(sub).count() - 1
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val mmap = mutableMapOf<String, Int>()

        for (i in s.split(sub)) {
            if (mmap.containsKey(i)) {
                mmap.put(i, mmap.getValue(i) + 1)
            } else {
                mmap.put(i, 1)
            }
        }
        return mmap
    }

    override fun isPalindrome(s: String): Boolean {
        return if (s == "")
            false
        else
            s.reversed().lowercase() == s.lowercase()
    }

    override fun invert(s: String): String {
        return s.reversed()
    }
}
